# импортируем нужные модули
import pygame
from random import randint 

# подключаем pygame
pygame.init()
window = pygame.display.set_mode((500,500)) # окно игры
back = (216, 235, 209) # цвет фона
window.fill(back) # закрашиваем фон

clock = pygame.time.Clock() # внутриигровые часы

BLACK = (0, 0, 0)
BLUE = (10, 254, 255)

# класс для карточек
class TextArea():
    # конструктор, задаем начальные параметры
    # место карточки x y, её ширина и высота width и height
    # и цвет color
    def __init__(self,x=0, y=0, width=10, height=10, color=None):
        self.rect = pygame.Rect(x, y, width, height) # прямоугольная область
        self.fill_color = color # цвет карточки
        self.titles = list() # список текстов карточки

    # добавление нового текста в список
    def add_text(self,text):
        self.titles.append(text)
    
    # установить текст для карточки
    def set_text(self, number=0 , fsize=12, text_color=BLACK):
        self.text = self.titles[number] # выбираем текст с номером number из списка текстов
        self.font = pygame.font.Font(None,fsize) # создание шрифта с размером fsize
        self.image = self.font.render(self.text, True, text_color) # картинка с текстом цвета text_color

    # отрисовка карточки и текста
    def draw(self, shift_x=0, shift_y=0):
        pygame.draw.rect(window, self.fill_color, self.rect) # заливаем цветом карточку
        window.blit(self.image, (self.rect.x + shift_x, self.rect.y + shift_y)) # отображаем текст со смещением вправо вниз

# карточка с вопросом
quest_card = TextArea(120, 100, 290, 70, BLUE)
quest_card.add_text('Вопрос') # начальная заглушка
# остальные вопросы
quest_card.add_text('Что изучаешь в Алгоритмике?')
quest_card.add_text('На каком языке говорят во Франции?')
quest_card.add_text('Что растёт на яблоне?')
quest_card.add_text('Что падает с неба во время дождя?')
quest_card.add_text('Что едят на ужин?')
# устанавливаем текст "Вопрос" размера 75
quest_card.set_text(0, 75)

# карточка с ответом
ans_card = TextArea(120, 240, 290, 70, BLUE)
ans_card.add_text('Ответ') # начальная заглушка
# остальные вопросы
ans_card.add_text('Python')
ans_card.add_text('Французский')
ans_card.add_text('Яблоки')
ans_card.add_text('Капли дождя')
ans_card.add_text('Жаркое с грибами')
# устанавливаем текст "Ответ" размера 75
ans_card.set_text(0, 75)

# отрисовываем карточки
quest_card.draw(10, 10)
ans_card.draw(10, 10)

"""  Игровой цикл """
while True:
    # перебираем список событий
    for event in pygame.event.get():
        # ищем события "Нажатие клавиши"
        if event.type == pygame.KEYDOWN:
            # если нажата клавиша Q
            if event.key == pygame.K_q:
                num = randint(1, len(quest_card.titles)- 1) # генерируем случайный номер текста
                quest_card.set_text(num, 25) # устанавливаем текст с этим номером
                quest_card.draw(10,25) # отрисовываем карточку 
            # если нажата клавиша A
            if event.key == pygame.K_a:
                num = randint(1, len(ans_card.titles)- 1) # генерируем случайный номер текста
                ans_card.set_text(num,25) # устанавливаем текст с этим номером
                ans_card.draw(10,25) # отрисовываем карточку
    # обновляем все окно игры
    pygame.display.update()
    clock.tick(30)